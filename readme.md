<h1>Personal worktools</h1>
<p>Simple linux command runner</p>
<h2>Requirements:</h2>
<p><b>Redis++</b></p>
<p> - https://github.com/sewenew/redis-plus-plus</p>
<h2>Installation:</h2>
<p>- clone this project</p>
<p>- clone & install requirements</p>
<p>- cd into project folder \build</p>
<p>- run <i>cmake install</i> .</p>
<p>- run <i>make</i></p>
<p>- copy config reference into <i>/usr/lib/t.conf</i> and configure project</p>

