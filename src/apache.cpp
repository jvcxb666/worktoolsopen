#include "apache.h"

using namespace Application;
using namespace Utils;

const std::map<std::string,int> map;
std::string containerName;

Apache::Apache() {
    containerName = ConfigProvider::getConfigVariableStatic("apache_container_name");
}

void Apache::execute(std::string* argument)
{
    switch(getArgMapingKey(argument))
    {
        case 1:
            restart();
            break;
        case 2:
            defaultExecute("a2enmod rewrite");
            break;
        case 3:
            defaultExecute("tail -f /var/log/apache2/error.log");
            break;
        case 4:
            defaultExecute("tail -f /var/log/apache2/error.log |grep Fatal");
            break;
        case 5:
            defaultExecute("composer install --ignore-platform-reqs --no-scripts");
            break;
        case 6:
            defaultExecute("bash");
            break;
        case 7:
            defaultExecute("php cli-runner.php --action='saveFixedRoleJson'");
            break;
        default:
            std::cout << "Argument '" << *argument << "' is not supported" << std::endl;
            exit(0);
    }
}

int Apache::getArgMapingKey(std::string* argument)
{
    try{
        return map.at(*argument);
    }catch(std::exception& e){
        return -1;
    }
}

void Apache::restart()
{
    std::string* cmd = new std::string("sudo docker restart ");
    cmd->append(containerName);
    Command::execute(cmd);
    delete cmd;
}

void Apache::defaultExecute(std::string command) {
    std::string* cmd = new std::string("sudo docker exec -it ");
    cmd->append(containerName);
    cmd->append(" ");
    cmd->append(command);

    Command::execute(cmd);

    delete cmd;
}