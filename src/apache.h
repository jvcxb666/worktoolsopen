#include <iostream>
#include <string>
#include <map>
#include "command.h"
#include "config_provider.h"

#ifndef APACHE_H_
#define APACHE_H_

namespace Application
{
    class Apache
    {
        public:
            Apache();
            const std::map<std::string,int> map { {"-r",1}, {"-m",2}, {"-l",3}, {"-le",4}, {"-i",5}, {"-b",6}, {"-ar",7} };
            void execute(std::string* argument);
        private:
            int getArgMapingKey(std::string* argument);
            void restart();
            void defaultExecute(std::string command);
            std::string containerName;
    };
}

#endif
