#include "cacher.h"

using namespace Utils;
using namespace sw::redis;

void Cacher::clearCache(std::string* IPAddress)
{
    try{

        std::string* connectionString = new std::string("tcp://");
        connectionString->append(*IPAddress);
        connectionString->append(":");
        connectionString->append(ConfigProvider::getConfigVariableStatic("redis_container_port"));

        auto client = Redis(*connectionString);
        delete connectionString;
        std::vector<std::string> keys = getKeys();

        for(std::string key : keys) {
            if(key.find("PHPREDIS_SESSION") != 0){
                client.del(key);
            }
        }

    }catch(const std::exception& e){
        std::cout << "Error: " << e.what() << std::endl;
    }
}

std::vector<std::string> Cacher::getKeys()
{
    std::vector<std::string> keys;
    std::string* cmd = new std::string("sudo docker exec -it ");
    cmd->append(ConfigProvider::getConfigVariableStatic("redis_container_name"));
    cmd->append(" redis-cli keys '*'");

    std::string keysString = Command::getResult(cmd);
    delete cmd;

    if(keysString.length() > 0){
        
        std::istringstream iss(keysString);

        for (std::string token; std::getline(iss, token, '"'); )
        {
            if (token.length() > 7) keys.push_back(std::move(token));
        }

    }
    
    return keys;
}
