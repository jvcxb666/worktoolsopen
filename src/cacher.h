#include <iostream>
#include <string>
#include <sw/redis++/redis++.h>
#include "command.h"
#include "config_provider.h"

#ifndef CACHER_H_
#define CACHER_H_

namespace Utils
{
    class Cacher
    {
        public:
            void clearCache(std::string* IPAddress);
        private:
            std::vector<std::string> getKeys();
            void removeAllButSession();
    };
}

#endif