#include "cleaner.h"

using namespace Application;
using namespace Utils;

Cacher cache;
const std::map<std::string,int> map;

void Cleaner::execute(std::string* argument)
{
    switch(getArgMapingKey(argument)){
        case 1:
            runReset();
            runClearCache();
            break;
        case 2:
            runReset();
            break;
        case 3:
            runClearCache();
            break;
        default:
            std::cout << "Argument '" << *argument << "' is not supported" << std::endl;
            exit(0);
    }
}

int Cleaner::getArgMapingKey(std::string* argument)
{
    try{
        return map.at(*argument);
    }catch(std::exception& e){
        return -1;
    }
}

void Cleaner::runReset()
{
    std::string* cmd = new std::string("sudo docker exec -it ");
    cmd->append(ConfigProvider::getConfigVariableStatic("apache_container_name"));
    cmd->append(" php cli-runner.php --action='migration-quickRepair'");
    std::cout << "Running reset" << "\n";
    Command::executeWithoutOutput(cmd);
    std::cout << "Done!" << "\n";
    delete cmd;
}

void Cleaner::runClearCache()
{
    std::string* cmd = new std::string("sudo docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ");
    cmd->append(ConfigProvider::getConfigVariableStatic("redis_container_name"));
    std::string redisIp = Command::getResult(cmd);
    delete cmd;

    if(redisIp.length() > 0){
        std::cout << "Cleaning cache" << "\n";
        cache.clearCache(&redisIp);
        std::cout << "Done!" << "\n";
    }
}