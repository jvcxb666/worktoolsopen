#include <iostream>
#include <string>
#include <map>
#include "command.h"
#include "cacher.h"
#include "config_provider.h"

#ifndef CLEANER_H_
#define CLEANER_H_

namespace Application
{
    class Cleaner
    {
        public:
            const std::map<std::string,int> map { {"-a",1}, {"-r",2}, {"-c",3} };
            void execute(std::string* argument);
        private:
            int getArgMapingKey(std::string* argument);
            void runReset();
            void runClearCache();
    };
}

#endif