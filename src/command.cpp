#include "command.h"

using namespace Utils;

void Command::execute(char* command)
{
    system(command);
}

void Command::execute(std::string command)
{
    system(command.c_str());
}

void Command::execute(std::string* command)
{
    system(command->c_str());
}

void Command::executeWithoutOutput(std::string command) {
    getResult(command.c_str());
}

void Command::executeWithoutOutput(std::string* command) {
    getResult(command->c_str());
}

std::string Command::getResult(std::string* command)
{
    return getResult(command->c_str());
}

std::string Command::getResult(const char* command)
{
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(command, "r"), pclose);
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        if(buffer.data() != "/n")
        result += buffer.data();
    }
    return result.erase(result.length()-1);
}