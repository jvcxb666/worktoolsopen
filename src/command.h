#include <iostream>
#include <string>
#include <memory>
#include <array>

#ifndef COMMAND_H_
#define COMMAND_H_

namespace Utils
{
    class Command{
        public:
            static void execute(char* command);
            static void execute(std::string command);
            static void execute(std::string* command);
            static void executeWithoutOutput(std::string command);
            static void executeWithoutOutput(std::string* command);
            static std::string getResult(const char* command);
            static std::string getResult(std::string* command);
    };
}

#endif