#include "config_provider.h"

using namespace Utils;

std::map<std::string,std::string> configMap;
static const std::string CONFIG_FILE = "/usr/lib/t.conf";

ConfigProvider::ConfigProvider() {
    readConfig();
}

std::string ConfigProvider::getConfigVariableStatic(std::string key) {
    std::ifstream* stream = new std::ifstream;

    stream->open(CONFIG_FILE);

    if(stream->is_open()) {
        std::string* line = new std::string;

        while(std::getline(*stream,*line)){
            if(line->length() > 1 && line->substr(0,1) != "#" && line->find("=") != std::string::npos){
                if(line->substr(0,line->find("=")).find(key) != std::string::npos){
                    return line->substr(line->find("=") + 1 ,line->length());
                }
            }
        }

        stream->close();
        delete line;
    }

    delete stream;

    std::cout << "FATAL ERROR: config key " << key << " does not exist \n";
    exit(0);
}

std::string ConfigProvider::getConfigVariableStatic(std::string* key) {
    return getConfigVariableStatic(*key);
}


std::string ConfigProvider::getConfigVariable(std::string key) {
    try{
        return configMap.at(key);
    }catch(std::exception e){
        std::cout << "FATAL ERROR: config key " << key << " does not exist \n";
        exit(0); 
    }
}

std::string ConfigProvider::getConfigVariable(std::string* key) {
    return getConfigVariable(*key);
}

void ConfigProvider::readConfig() {
    std::ifstream* stream = new std::ifstream;

    stream->open(CONFIG_FILE);

    if(stream->is_open()) {
        std::string* line = new std::string;

        while(std::getline(*stream,*line)){
            if(line->length() > 1 && line->substr(0,1) != "#" && line->find("=") != std::string::npos){
                configMap[line->substr(0,line->find("="))] = line->substr(line->find("=") + 1 ,line->length());
            }
        }

        stream->close();
        delete line;
    }

    delete stream;
}