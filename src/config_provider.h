#include <string>
#include <map>
#include <fstream>
#include <iostream>

#ifndef CONFIG_PROVIDER_H_
#define CONFIG_PROVIDER_H_

namespace Utils {
    class ConfigProvider {
        private:
            std::map<std::string,std::string> configMap;
            void readConfig();
        public: 
            ConfigProvider();
            std::string getConfigVariable(std::string key);
            std::string getConfigVariable(std::string* key);
            static std::string getConfigVariableStatic(std::string key);
            static std::string getConfigVariableStatic(std::string* key);
    };
}

#endif