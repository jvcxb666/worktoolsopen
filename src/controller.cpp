#include "controller.h"

using namespace Application;

Cleaner cleaner;
Apache apache;
Ssh ssh;

const std::map<std::string,int> map;

void Controller::run(char* args[])
{
    std::string command(args[1]);
    std::string parameter(args[2]);
    std::string clear(args[3] ? args[3] : "");

    executeCommand(&command, &parameter);

    if(!clear.empty() && clear == "--c") system("clear");
}

void Controller::showHelp()
{
    //sensitive info
}

void Controller::executeCommand(std::string* command, std::string* parameter)
{
    switch(getCommandMapingKey(command)){
        case 1:
            cleaner.execute(parameter);
            break;
        case 2:
            apache.execute(parameter);
            break;
        case 3:
            ssh.execute(parameter);
            break;
        default:
            std::cout << "Command '" << *command << "' not found" << std::endl;
            exit(0);
    }
}

int Controller::getCommandMapingKey(std::string* command)
{
    try{
        return map.at(*command);
    }catch(std::exception& e){
        return -1;
    }
}