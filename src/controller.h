#include <iostream>
#include <string>
#include <map>
#include "cleaner.h"
#include "apache.h"
#include "ssh.h"

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

namespace Application
{
    class Controller
    {
        public:
            const std::map<std::string,int> map { {"clear",1}, {"apache",2}, {"ssh",3} };
            void run(char* args[]);
            void showHelp();
        private:
            int getCommandMapingKey(std::string* command);
            void executeCommand(std::string* command, std::string* parameter);
    };
}

#endif