#include <iostream>
#include "controller.h"

using namespace Application;

Controller controller;

int main(int argc, char *argv[])
{
    if(argc > 1 && *argv[1] == *"help"){

        controller.showHelp();
        exit(0);
    }
    if(argc < 3){
        std::cout << "Too few arguments passed in" << std::endl << "Run `help` for commands list" << std::endl;
        exit(0);
    }
    controller.run(argv);
    exit(0);
}