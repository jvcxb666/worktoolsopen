#include "ssh.h"

using namespace Application;
using namespace Utils;

const std::map<std::string,int> map;

void Ssh::execute(std::string* argument) {
    switch (getArgMapingKey(argument))
    {
    case 1:
        Command::execute(
            buildConnectionString(
                ConfigProvider::getConfigVariableStatic("c_ssh_user"),
                ConfigProvider::getConfigVariableStatic("c_ip_addr") 
            )
        );
        break;
    case 2:
         Command::execute(
            buildConnectionString(
                ConfigProvider::getConfigVariableStatic("b_ssh_user"),
                ConfigProvider::getConfigVariableStatic("b_ip_addr") 
            )
        );
        break;
    case 3:
         Command::execute(
            buildConnectionString(
                ConfigProvider::getConfigVariableStatic("l_ssh_user"),
                ConfigProvider::getConfigVariableStatic("l_ip_addr") 
            )
        );
        break;
    case 4:
         Command::execute(
            buildConnectionString(
                ConfigProvider::getConfigVariableStatic("t_ssh_user"),
                ConfigProvider::getConfigVariableStatic("t_ip_addr") 
            )
        );
        break;
    default:
        std::cout << "Argument '" << *argument << "' is not supported" << std::endl;
        exit(0);
    }
}

int Ssh::getArgMapingKey(std::string* argument) {
    try {
        return map.at(*argument);
    } catch (const std::exception & ex) {
        return -1;
    }
}

std::string Ssh::buildConnectionString(std::string user,std::string address) {
    std::string cmd = "sudo ssh ";
    cmd.append(user);
    cmd.append("@");
    cmd.append(address);
    return cmd;
}