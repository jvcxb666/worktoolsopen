#include <map>
#include <string>
#include <iostream>
#include "command.h"
#include "config_provider.h"

#ifndef SSH_H_
#define SSH_H_

namespace Application{
    class Ssh
    {
        public:
            const std::map<std::string,int> map { {"-c",1} , {"-b",2} , {"-l",3}, {"-t",4} };
            void execute(std::string* argument);
        private:
            int getArgMapingKey(std::string* argument);
            std::string buildConnectionString(std::string user,std::string address);
    };
}

#endif

